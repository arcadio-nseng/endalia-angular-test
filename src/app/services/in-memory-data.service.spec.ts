import { TestBed } from '@angular/core/testing';

import { InMemoryDataService } from './in-memory-data.service';
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('InMemoryDataService', () => {
  let service: InMemoryDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient]
    });
    service = TestBed.inject(InMemoryDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
