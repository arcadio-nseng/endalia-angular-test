import { TestBed } from '@angular/core/testing';

import { CanActivateEmployee } from './can-activate-employee.service';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";

describe('CanActivateEmployeeService', () => {
  let service: CanActivateEmployee;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      providers: [HttpClient]
    });
    service = TestBed.inject(CanActivateEmployee);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
