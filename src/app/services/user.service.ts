import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, firstValueFrom, of} from "rxjs";
import {User} from "../types";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userKey = 'user';

  constructor(private http: HttpClient) {}

  async login(email: string, password: string): Promise<boolean> {

    const user$ = this.http.get<User[]>(`api/users/`)
      .pipe(catchError(err => {
        console.log(err);
        return of([]);
      }))

    const users = await firstValueFrom(user$);
    const user = users.find(u => u.email === email.toLowerCase().trim())

    if (user && user.password === password) {
      sessionStorage.setItem(this.userKey, `${users[0].id}`)
      return Promise.resolve(true);
    }

    return Promise.resolve(false);

  }

  logout(): void {
    sessionStorage.removeItem(this.userKey);
  }

  get loggedInd(): boolean {
    return sessionStorage.getItem(this.userKey) !== null;
  }

}
