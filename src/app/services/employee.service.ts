import { Injectable } from '@angular/core';
import {Employee} from "../types";
import {catchError, Observable, of, tap} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getEmployees(query: string): Observable<Employee[]> {

    let apiUrl = 'api/employees/';
    if (query.length > 0) apiUrl += `?firstName=${query}`;

    return this.http.get<Employee[]>(apiUrl)
      .pipe(
        tap(employees => employees.sort(this.compareEmployees)),
        catchError(err => {
          console.log(err);
          return of([]);
        })
      )
  }

  private compareEmployees (a: Employee, b: Employee) {
    if (a.lastName < b.lastName) return -1;
    if (a.lastName > b.lastName) return 1;
    return 0;
  }

}
