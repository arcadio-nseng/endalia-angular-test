import { Injectable } from '@angular/core';
import {InMemoryDbService} from "angular-in-memory-web-api";
import {Employee, User} from "../types";

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  createDb(): {employees: Employee[], users: User[]} {
    return {
      employees: [
        {
          lastName: 'AGUIRRE LEÓN', firstName: 'Verónica',
          position: 'Operario sección 1',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp3.jpg'
        },
        {
          lastName: 'AGUIRRE RIVERA', firstName: 'Miguel Ángel',
          position: 'Dirección comercial',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp2.jpg'
        },
        {
          lastName: 'ALCALÁ HERRERA', firstName: 'Carlos',
          position: 'Auditor de medioambiente / Encargado de producción',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp4.jpg'
        },
        {
          lastName: 'ALCALÁ ORDOÑEZ', firstName: 'Ángela',
          position: 'Administrativo/a',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp5.jpg'
        },
        {
          lastName: 'ALVAREZ PINEDA', firstName: 'Laura',
          position: 'Auditor de calidad',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp7.jpg'
        },
        {
          lastName: 'ANTÓN TERUEL', firstName: 'Pedro',
          position: 'Operario sección 2 Turno tarde',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp6.jpg'
        },
        {
          lastName: 'APARICIO HERRERO', firstName: 'Vicente',
          position: 'Cordinador de ventas zona norte',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp8.jpg'
        },
        {
          lastName: 'ARIZA FRANCO', firstName: 'María Paula',
          position: 'Responsable de proyecto',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp9.jpg'
        },
        {
          lastName: 'ARIZA IDALGO', firstName: 'Lucía',
          position: 'Auditor de medio ambiente',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp10.jpg'
        },
        {
          lastName: 'IDALGO JIMÉNEZ', firstName: 'Antonio',
          position: 'Responsable de desarrollo de software',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp11.jpg'
        },
        {
          lastName: 'MENDEZ ARIZA', firstName: 'Laura',
          position: 'Jefa de producción',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp12.jpg'
        },
        {
          lastName: 'PLAZA CALVO', firstName: 'Catalina',
          position: 'Cordinadora logística',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp13.jpg'
        },
        {
          lastName: 'SÁNCHEZ SOTELO', firstName: 'Princesa',
          position: 'Responsable de equipos',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp14.jpg'
        },
        {
          lastName: 'ABAD JIMÉNEZ', firstName: 'Ignacio',
          position: 'Responsable de desarrollo de RRHH',
          phone: '61744290', email: 'prueba@endalia.com',
          photo: '/assets/images/emp1.jpg'
        }
      ],
      users: [
        {id: 101, email: 'endalia@endalia.com', password: '12345'}
      ]
    };
  }



  constructor() { }
}
