import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CanActivateEmployee} from "./services/can-activate-employee.service";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', loadChildren: () => import('./pages/login-page/login-page.module').then(m => m.LoginPageModule) },
  {
    path: 'empleados',
    loadChildren: () => import('./pages/employees-page/employees-page.module').then(m => m.EmployeesPageModule) ,
    canActivate: [CanActivateEmployee]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
