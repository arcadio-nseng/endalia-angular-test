export declare type User = {
  id: number;
  email: string;
  password: string;
}

export declare type Employee = {
  phone: string;
  firstName: string;
  lastName: string;
  position: string;
  email: string;
  photo: string;
}
