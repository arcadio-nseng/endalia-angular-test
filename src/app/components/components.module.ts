import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeCardComponent } from './employee-card/employee-card.component';



@NgModule({
  declarations: [

    EmployeeCardComponent
  ],
    exports: [
        EmployeeCardComponent
    ],
  imports: [
    CommonModule,
  ]
})
export class ComponentsModule { }
