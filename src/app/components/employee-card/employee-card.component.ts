import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.css']
})
export class EmployeeCardComponent implements OnInit {

  @Input() firstName!: string
  @Input() lastName!: string
  @Input() position!: string
  @Input() phone!: string
  @Input() email!: string
  @Input() photo!: string
  constructor() { }

  ngOnInit(): void {
  }

}
