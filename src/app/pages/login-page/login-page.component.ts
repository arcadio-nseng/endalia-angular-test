import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  email = '';
  password = '';

  constructor(
    private userService: UserService,
    private router: Router,
    titleService: Title
  ) {
    titleService.setTitle('Login | Endalia HR')
  }

  ngOnInit(): void {


    if (this.userService.loggedInd) {
      this.router.navigateByUrl('/empleados').then();
    }

  }

  login(event: Event) {

    event.preventDefault();

    this.userService.login(this.email, this.password)
      .then(success => {
        if (success) this.router.navigateByUrl('/empleados').then()
        else alert('Email o contraseña no válidos')
      })

  }

}
