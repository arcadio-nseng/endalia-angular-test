import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../types";
import {BehaviorSubject, Observable} from "rxjs";
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators'
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-employees-page',
  templateUrl: './employees-page.component.html',
  styleUrls: ['./employees-page.component.css']
})
export class EmployeesPageComponent implements OnInit {

  employees$!: Observable<Employee[]>;
  private searchQuery = new BehaviorSubject<string>('')

  constructor(
    private user: UserService,
    private router: Router,
    private employeeService: EmployeeService,
    private titleService: Title
  ) {
    titleService.setTitle('Empleados | Endalia HR')
  }

  ngOnInit(): void {

    this.employees$ = this.searchQuery.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((query: string) => this.employeeService.getEmployees(query))
    );

  }

  search(query: string) {
    this.searchQuery.next(query);
  }

  logout() {
    this.user.logout();
    this.router.navigateByUrl('/login').then()
  }

}
