import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesPageRoutingModule } from './employees-page-routing.module';
import { EmployeesPageComponent } from './employees-page.component';
import {ComponentsModule} from "../../components/components.module";


@NgModule({
  declarations: [
    EmployeesPageComponent
  ],
  imports: [
    CommonModule,
    EmployeesPageRoutingModule,
    ComponentsModule
  ]
})
export class EmployeesPageModule { }
