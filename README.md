# Pureba técnica Angular

Las imágenes, tipografía y demás elementos de estilo se han seleccionado de forma arbitraria pero siempre intentando ser lo más fiel posible al diseño planteado. Por simplicidad he utilizado ``Tailwind CSS`` para estilizar los componentes

## 1. Login

La página de login solo reconocerá al usuario de ejemplo `endalia@endalia.com` con la contraseña `12345`.

## 2. Directorio de empleados

El cuadro de búsqueda filtra los empleados por su `nombre`


